---
title: "project big data"
author: "Elad Israeli and Liel Mushiev"
date: "15.09.2019"
output: html_document
---
```{r setup, cache = F, echo = F, message = F, warning = F, tidy = F}
# make this an external chunk that can be included in any file
library(knitr)
options(width = 70)
opts_chunk$set(message = F, error = F, warning = F, collapse=T, comment = "##", fig.align = 'center', dpi = 100, tidy = F, cache = T, cache.path = '.cache/', fig.path = 'fig/')

options(xtable.type = 'html')
knit_hooks$set(inline = function(x) {
  if(is.numeric(x)) {
    round(x, getOption('digits'))
  } else {
    paste(as.character(x), collapse = ', ')
  }
})
knit_hooks$set(plot = knitr:::hook_plot_html)
```

```{r}
knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(ggplot2)
library(readxl)
library(datasets)
library(wesanderson)
library(reshape2)
library(tidyverse)
library(pscl)
library(ROCR)
library(ggfortify)
```

```
knitr::purl("project_proposal.Rmd", output = "Elad_Israeli-Proposal.R")
```

---
1.  Introduction to the data set:
      We found the data online. We were inspired for this particular theme because of the recent elections. We wanted to examine the relations between the          region and the main chosen party at that place.
      We used data-base we found online regarding to the 2016-USA-elections.
      The data was presented in OpenDataSoft website.
      
2.  Research question:
      According to preliminary information we were informed with, some may claim to have a relation between the size of a county and the main chosen party in       this area (Republican/Democrats).
      We would like to examine the connection mentioned under the assumption that in big-counties there's a tendency for a majority of Democrats, and in            small-counties a tendency for a majority of Republicans.

3.  Observations:
      We have 3,144 rows that stand for counties within the 52 states of the US.
      
4.  Structure of database:
      Our database is a table with rows as mentioned above, and different columns as followed (we have many columns we'll refer the relevant only):
      - State
      - County
      - Votes
      - Democrats 08 (votes) - num of votes for the Democrats in the election of 2008
      - Democrats 12 (votes) - num of votes for the Democrats in the election of 2012
      - Republicans 08 (votes) - num of votes for the Republicans in the election of 2008
      - Republicans 12 (votes) - num of votes for the Republicans in the election of 2012
      - Democrats 16 (votes) - num of votes for the Democrats in the election of 2016
      - Republicans 16 (votes) - num of votes for the Republicans in the election of 2016
      - Less than High-School Diploma - precent of population with no high-school diploma
      - At Least Bachelors's Degree - precent of population with B.A degree
      - Graduate Degree - precent of population with M.A degree
      - White (Not Latino) population - precent of white people in the total population
      - African-American population - precent of African-American people in the total population
      - Native-American - precent of Native-American people in the total population
      - Asian-American - precent of Asian-American people in the total population
      - Latino - precent of Latin people in the total population
      - Other population - the remained precent of other race population
      - Children Under 6 living in Poverty
      - Adults 65 and Older Living in Poverty
      - Total Population - total population of the county
      - Median Earnings
      - Median Age
      - Uninsured - proportion of the total population that doesn't have insurance
      - Unemployment - proportion of the total population that has no job
      - Votes 16 Trumpd - votes for Trump in the election of 2016
      - Votes 16 Clintonh - votes for Clinton in the election of 2016
      
5. Data Source:
      The data was collected from the internet.
      link: https://public.opendatasoft.com/explore/dataset/usa-2016-presidential-election-by-county/export/?disjunctive.state
      (we downloaded the file as an Excel format)
      
6. Relevant Summary Statistics:
      We've examined different connections between the columns to get as many conclusions as possible about predicting the results of the next elections.
      The conclusions are as followed in the next few graphs and statistics exams.
      
---



```{r}
mydata<-read_excel("usa.xls")
```

## Clean the data

```{r}
# taking only relavent column
mydata1<-mydata[,c(1,4,6:12,19,21:33,52,65,66)]
mydata1<-mydata1[!mydata1$State=='Alaska',]
for (i in 1:length(mydata1$County)){   #need to be changed if we change the num of columns
  for (j in 1:26) {
    if(is.na(mydata1[i,j]))
      mydata1[i,j]<-0
  }
}
mydata1$`Republicans 2016(votes)`<-0
mydata1$`Democrats 2016(votes)`<-0
for (i in 1:length(mydata1$County)) {
  mydata1$`Republicans 2016(votes)`[i]<-as.numeric(mydata1[i,3]*mydata1[i,8]*0.01)
  mydata1$`Democrats 2016(votes)`[i]<-as.numeric(mydata1[i,3]*mydata1[i,9]*0.01)
}
```

```{r}
VOTES<-aggregate(mydata1$Votes, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
Democrats_08<-aggregate(mydata1$`Democrats 08 (Votes)`, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
DEM08<-Democrats_08$x
Republicans_08<-aggregate(mydata1$`Republicans 08 (Votes)`, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
REP08<-Republicans_08$x
Democrats_12<-aggregate(as.numeric(mydata1$`Democrats 12 (Votes)`), by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
DEM12<-Democrats_12$x
Republicans_12<-aggregate(mydata1$`Republicans 12 (Votes)`, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
REP12<-Republicans_12$x
Republicans_2016<-aggregate(mydata1$`Republicans 2016(votes)`, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
REP16<-Republicans_2016$x
Democrats_2016<-aggregate(mydata1$`Democrats 2016(votes)`, by=list(Category=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
DEM16<-Democrats_2016$x
mydata1$"binary"<- 0
for(i in 1:3114){
  if(mydata1$`Republicans 2016`[i]>mydata1$`Democrats 2016`[i])
    mydata1$binary[i]<-1
  else
    mydata1$binary[i]<-0
}
mydata1$"binary total population" <-0
for(i in 1:3114){
  if(mydata1$`Total Population`[i]>100000)
    mydata1$'binary total population'[i]<-1
  else
    mydata1$'binary total population'[i]<-0
}


```


```{r}
dem08<-sum(DEM08)
dem12<-sum(DEM12)
dem16<-sum(DEM16)
rep08<-sum(REP08)
rep12<-sum(REP12)
rep16<-sum(REP16)


SUMDATA<-data.frame(x = c("2008","2008","2012","2012","2016","2016"),y=c(dem08,rep08,dem12,rep12,dem16,rep16),z=c("democrat","republican"))

ggplot(SUMDATA,aes(x,y,fill=z))+
 geom_bar(stat="identity",position='dodge',ylab='number of voutes',main='number of voutes vs years')

```
we can see that the Democrats' votes rate is decreasing. 
According to some people, it is a result of disappointment because of Obama, and a lack of willing to choose a woman. 
In the other hand, there's a slight increase in the Repulicans' votes rate, and though, despite of the fact they got less votes they still won the elections. 
It is an interesting fact to witness that in order to win you are not obligated to have more votes.
```

```{r}
TOTAL<-aggregate(list(Total.Population=mydata1$`Total Population`,Votes=mydata1$Votes,totdem16=mydata1$`Democrats 2016(votes)`,totrep16=mydata1$`Republicans 2016(votes)`), by=list(State=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
for (i in 1:50) {
  if(TOTAL$totrep16[i]>TOTAL$totdem16[i])
  TOTAL$winner[i]<-"republican"
  else
    TOTAL$winner[i]<-"democrat"
}
ggplot(TOTAL,aes(x=State,y=Votes,size=Total.Population,col=winner))+geom_point()+theme(axis.text.x = element_text(angle = 90))

```
Here, we can see the distribution of voters in different states.
The circle size stands for the size of the state; its color stands for the majority public's votes (per state) and we have the total amount of votes for each state.
For example:
We can see that in the state of California, we can see the circle is pretty big - which indicates to have a lot of population.
There are almost 12,500,000 votes; and it's red - which indicates that most of the votes are for the democrats.

```{r}

#How the population is composed


mydata1$white<-0
mydata1$african_american<-0
mydata1$Native_american<-0
mydata1$asian<-0
mydata1$other<-0
mydata1$Latino<-0
for (i in 1:length(mydata1$County)) {
  mydata1$white[i]<-as.integer(mydata1[i,23]*mydata1[i,15]*0.01)
  mydata1$african_american[i]<-as.integer(mydata1[i,23]*mydata1[i,16]*0.01)
  mydata1$Native_american[i]<-as.integer(mydata1[i,23]*mydata1[i,17]*0.01)
  mydata1$asian[i]<-as.integer(mydata1[i,23]*mydata1[i,18]*0.01)
  mydata1$other[i]<-as.integer(mydata1[i,23]*mydata1[i,19]*0.01)
   mydata1$Latino[i]<-as.integer(mydata1[i,23]*mydata1[i,20]*0.01)
}
Population_distribution<-aggregate(list(Total.Population=mydata1$`Total Population` ,White=mydata1$white,African_American=mydata1$african_american,NAtive_American=mydata1$Native_american,Asian=mydata1$ asian,LAtino=mydata1$Latino,other=mydata1$other), by=list(State=mydata1$State), FUN=sum, simplify = TRUE, drop = TRUE)
nempopulation<-Population_distribution[,c(1,3,4,5,6,7,8)]
nemdata<-melt(data = nempopulation)
ggplot(data=nemdata,aes(x=State,y=value,fill=as.factor(variable)))+geom_bar(stat="identity",position = "fill")+theme(axis.text.x = element_text(angle = 90))
```
In this graph we colored the columns.
The size of the color shows the partial ratio of a specific population out of the total.

```{r}
qplot(x=State,y=Total.Population, data=Population_distribution, main = ,
      xlab = ,ylab =  )+theme(axis.text.x = element_text(angle = 90))


```




```{r}
poverty<-aggregate(list(Children_Under_6Living_in_Poverty=mydata1$`Children Under 6 Living in Poverty`,Adults_65_and_Older_Living_in_Poverty=mydata1$`Adults 65 and Older Living in Poverty`), by=list(State=mydata1$State), FUN=mean, simplify = TRUE, drop = TRUE)
ggplot(poverty, aes(x=State, y=Children_Under_6Living_in_Poverty, color=Adults_65_and_Older_Living_in_Poverty,main="poverty")) + geom_point(aes(shape=TOTAL$winner))+theme(axis.text.x = element_text(angle = 90))+scale_color_gradientn(colours = rainbow(5))



```
Here we have the distribution of the votes in each state, according to population's attribute:
The more purple the color is the bigger the prcentage of eldery living in poverty (65+ y.o).
The higher the shape (circle/triangle) the bigger the prcentage of children living in poverty (6- y.o).
The shape indicates the winning party in the state (circle for democrats, triangle for republicans).


```{r}
  meanofMedian_Earnings_2010<-mean(mydata1$`Median Earnings 2010`)
JOB<-aggregate(list(Median_Earnings_2010=mydata1$`Median Earnings 2010`,UNinsured=mydata1$Uninsured,UNemployment=mydata1$Unemployment), by=list(State=mydata1$State), FUN=mean, simplify = TRUE, drop = TRUE)
JOB$winner<-TOTAL$winner
ggplot(JOB, aes(x=State, y=Median_Earnings_2010, color=UNinsured,main="job")) + geom_point(aes(shape=TOTAL$winner))+theme(axis.text.x = element_text(angle = 90))+scale_color_gradientn(colours = rainbow(3))+geom_smooth(method="lm",se=F)+geom_hline(yintercept = 25437)

```
In this graph we have another distribution of the population's votes:
The more 'cold' the color is (meaning - goes blue), the bigger the prcentage of uninsured civilians in the state.
On the left axis we can see the median earnings in 2010.
The shape indicates the same as in the previous graph.
Here we wanted to check the relation between the socio-economic state of people in one country and their elections.





```{r}
states <- map_data("state")
ggplot(data = states) + 
  geom_polygon(aes(x = long, y = lat, fill = region, group = group), color = "white") + 
  coord_fixed(1.3) +
  guides(fill=FALSE)  # do this to leave off the color legend

```
Here we have a map showing the different countries that elect in the US.


```{r F}


map(database= "state", col="green", fill=FALSE)
map(database= "usa", col="#FFFFFF", fill=FALSE,add=TRUE, lwd=1.5)   # the overwrite of boundary
map(database= "usa", col="#AAAAAA22", fill=TRUE,lty=0,add=TRUE)   # note the transparency
text(x=state.center$x, y=state.center$y, state.abb, cex=0.5)
```
Here we have a map naming the different countries that elect in the US.

```{r}
map(database= "state", col="green", fill=FALSE)
map(database= "usa", col="#FFFFFF", fill=FALSE,add=TRUE, lwd=1.5)   # the overwrite of boundary
map(database= "usa", col="#AAAAAA22", fill=TRUE,lty=0,add=TRUE)   # note the transparency
text(x=state.center$x, y=state.center$y, state.abb, cex=0.5)
democrats=c("ILLINOIS", "KANSAS","LOUISIANA","MAINE","MICHIGAN","MINNESOTA","MISSISSIPPI" )

democrats <- filter(TOTAL, winner == 'democrat')
republicans <- filter(TOTAL, winner == 'republican')
r <- c(republicans$State)
d <- c(democrats$State)
map(database = "state", regions = d, col = "blue", fill = TRUE, add = TRUE) #we don't must to write 'add' here because it is the first condition of color
map(database = "state", regions = r, col = "red", fill = TRUE, add = TRUE) #here we want to combine the two results we've got so we'll keep the 'add'
```
This is the final map of the US according to the 2016-elections - painting red the areas in which the Republican won, and blue for the Democrats.

```{r}
## ------------------------------------------------------------------------
#county_mod_ldl=glm(`binary`~`binary total population`,data=mydata1,family=binomial)
#plot(jitter(binary, factor = 0.1) ~ `binary total population`, data = mydata1, pch = 20, 
#    ylab = "", xlab = "")
mydata1$rep1_dem0<- 0
for(i in 1:3114){
  if(mydata1$`Republicans 2016`[i]>mydata1$`Democrats 2016`[i])
    mydata1$rep1_dem0[i]<-1
  else
    mydata1$rep1_dem0[i]<-0
}
mydata1$"binary total population" <-0
for(i in 1:3114){
  if(mydata1$`Total Population`[i]>100000)
    mydata1$'binary total population'[i]<-1
  else
    mydata1$'binary total population'[i]<-0
}

small_county_and_Republicans_2016_win=0
small_county_and_Democrats_2016_win=0
Big_county_and_Republicans_2016_win=0
Big_county_and_Democrats_2016_win=0

for(i in 1:3114){
  if((mydata1$rep1_dem0[i]==1)&&(mydata1$`binary total population`[i]==0))
    small_county_and_Republicans_2016_win = small_county_and_Republicans_2016_win+1
  if((mydata1$rep1_dem0[i]==0) && (mydata1$`binary total population`[i]==0))
    small_county_and_Democrats_2016_win=small_county_and_Democrats_2016_win+1
  if((mydata1$rep1_dem0[i]==1) && (mydata1$`binary total population`[i]==1))
    Big_county_and_Republicans_2016_win=Big_county_and_Republicans_2016_win+1
  if((mydata1$rep1_dem0[i]==0) && (mydata1$`binary total population`[i]==1))
    Big_county_and_Democrats_2016_win=Big_county_and_Democrats_2016_win+1
}
dem08<-sum(DEM08)
dem12<-sum(DEM12)
dem16<-sum(DEM16)
rep08<-sum(REP08)
rep12<-sum(REP12)
rep16<-sum(REP16)


MDATA<-data.frame(x = c("small  Republicans","small  Democrats","big  Republicans","big  Democrats"),y=c(small_county_and_Republicans_2016_win,small_county_and_Democrats_2016_win,Big_county_and_Republicans_2016_win,Big_county_and_Democrats_2016_win),z=c("republican","democrat"))

ggplot(MDATA,aes(x,y,fill=z),ylab='number of county')+
  geom_bar(stat="identity",position='dodge')
```
Here we can already see the relation between the size of the county and the winning party in it.



MODEL FITTING:
We split the data into two chunks - training and testing set. The training set will be used to fit our model which we will be testing over the testing set.
```{r}
##The model
DataToTest<-mydata1[,c(10:13,21,22,25,26,37,31:36,23,14,30)]
DataToTest<-DataToTest %>% 
  rename(At_Least_Bachelors_s_Degree=`At Least Bachelors's Degree`,
 Graduate_Degree= `Graduate Degree`, 
  School_Enrollment=`School Enrollment`,
  Children_Under_6_Living_in_Poverty=`Children Under 6 Living in Poverty`,
  Adults_65_and_Older_Living_in_Poverty=`Adults 65 and Older Living in Poverty`,
  Total_Population=`Total Population`,
  Median_Earnings_2010=`Median Earnings 2010`,
 Less_Than_High_School_Diploma=`Less Than High School Diploma`,
binary_total_population= `binary total population`
 )
set.seed(779)
n=nrow(DataToTest)
indx = sample(n,round(0.2*n))
DataToTest.train<-DataToTest[-indx,]
DataToTest.test<-DataToTest[indx,]

```

```{r}
t.test(DataToTest$rep1_dem0,DataToTest$binary_total_population)
#� PVALUE ���� ��� �� ��� ��� ����� �� ���� PVALU �����
        
```
```{r}
plot(model1, 4)
plot(model1, 5)
```

```{r}
model1=glm(rep1_dem0~ ., data = DataToTest.train, family =binomial(link='logit') )
pR2(model1)
autoplot(model1)
```

The diagnostic plots show residuals in four different ways:

Residuals vs. Fitted. Used to check the linear relationship assumptions. 
A horizontal line, (without distinct patterns), indicates a linear relationship, which is good.

Normal Q-Q. Used to examine whether the residuals are normally distributed. 
It�s good if residuals points follow the straight dashed line.

Scale-Location (or Spread-Location). Used to check the homogeneity of variance of the residuals (homoscedasticity). 
Horizontal line with equally spread points is a good indication of homoscedasticity. 
This is not the case in our example, where we have a heteroscedasticity problem.

Residuals vs. Leverage. Used to identify influential cases, that is extreme values that might influence the regression results when included or excluded from the analysis. 
This plot will be described further in the next sections.




```{r}
summary(model1)
```
```{r}
#The following plots illustrate the Cook�s distance and the leverage of our model:
plot(model1, 4)
plot(model1, 5)
```

We see that we get an AIC of 1195.3 and now we try to get a better fitted model, so we use the model selection functions �step�:

```{r 2}
model2 = step(model1,direction = "backward" ,trace = 1)
plot(model2)
summary(model2)
model2
```
We did not receive a sufficient reduction

```{r}
model3=glm(rep1_dem0~1 , data = DataToTest.train,, family = binomial)
summary(model3)
```

```{r}
model4 = step(model3,scope =rep1_dem0 ~ Less_Than_High_School_Diploma + At_Least_Bachelors_s_Degree + 
    Graduate_Degree + School_Enrollment + Children_Under_6_Living_in_Poverty + 
    Adults_65_and_Older_Living_in_Poverty + Uninsured + Unemployment + 
    white + african_american + Native_american + asian + Latino + 
    Total_Population + Median_Earnings_2010,direction = "forward" ,trace = 1)
summary(model4)
```
And once again we were unable to improve our model.



Improving these models:
```{r}
DataToTestAfter1model<-DataToTest[,c(1:9)]
model4=glm(rep1_dem0~ ., data = DataToTestAfter1model, family =binomial(link='logit') )
plot(model4)
summary(model4)
```
We tried to take what showed srong connection and create a model out of them, but it did not improve the result.

```{r}
anova(model2, test="Chisq")
```


```{r}
library(randomForest)

model5 = randomForest(rep1_dem0~.,data = DataToTest)
plot(model5)
summary(model5)

```

PREDICTION:
In the steps above, we briefly evaluated the fitting of the model, now we would like to see how the model is doing when predicting y on a new set of data
```{r}
fitted.results<-predict(object = model2,newdata= DataToTest.test[,-9])
plot(fitted.results ,main= "predicted values of model2")

fitted.results <- ifelse(fitted.results > 0.5,1,0)

misClasificError <- mean(fitted.results != DataToTest.test$rep1_dem0)
print(paste('Accuracy',1-misClasificError))


```
The 0.898876404494382 accuracy on the test set is quite good as a result. 
We wish for a more accurate score, for better off running some kind of cross validation such as k-fold cross validation.

As a last step, we are going to plot the ROC curve and calculate the AUC (area under the curve) which are typical performance measurements for a binary classifier.
The ROC is a curve generated by plotting the true positive rate (TPR) against the false positive rate (FPR) at various threshold settings while the AUC is the area under the ROC curve. As a thumb rule, a model with good predictive ability should have an AUC closer to 1 (1 is ideal) than to 0.5.

```{r}
fitted.results<-predict(object = model2,newdata= DataToTest.test[,-9])



pr <- prediction(fitted.results, DataToTest.test$rep1_dem0)
prf <- performance(pr, measure = "tpr", x.measure = "fpr")
plot(prf)
auc <- performance(pr, measure = "auc")
auc <- auc@y.values[[1]]
auc
```
We can see that we recieved a value ~1; therefore we can assume there's connection between the size of the county (and more parmeters) that tested who will win in that county.